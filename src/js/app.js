//
//
function typing (){
      $('.type-it').typeIt({
       strings: ['Se el cambio que quieres ver en el mundo.','-Mahatma Gandhi.'],
       speed: 110,
       autoStart: true,
       loop:false,
       startDelay:500,
       cursorSpeed:2000,
       lifeLike:true,
       cursor:false})
 }
//
//
//
// }

$(document).ready(function(){
  setTimeout(function(){
    $('#preloader').fadeOut('slow');
    $('body').css({'overflow':'visible'});
    $(".fixed-action-btn").css("visibility","visible");
  },5000);
    $('.scrollspy').scrollSpy({
       scrollOffset:0
     });
     $('.modal').modal();
     $(".dropdown-button").dropdown();
     $('.button-collapse').sideNav({
         menuWidth: 300, // Default is 300
         edge: 'left', // Choose the horizontal origin
         closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
         draggable: true // Choose whether you can drag to open on touch screens
       });
       //  $('.tooltipped').tooltip({delay: 500},setTimeOut(function () {
       //     $('.tooltipped').tooltip('remove');
       //  },2000));
       $('.carousel').carousel();
       new WOW().init();
});
